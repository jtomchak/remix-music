//
//  MPMediaItem+PlayListItem.m
//  RemixMusic
//
//  Created by Jesse Tomchak on 8/9/15.
//  Copyright © 2015 Jesse Tomchak. All rights reserved.
//

#import "MPMediaItem+PlayListItem.h"

@implementation MPMediaItem (PlayListItem)

- (UIImage *)image {
    return [self.artwork imageWithSize:CGSizeMake(600, 600)];
}

@end
