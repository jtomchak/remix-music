//
//  SamplePlayListItem.m
//  RemixMusic
//
//  Created by Jesse Tomchak on 8/9/15.
//  Copyright © 2015 Jesse Tomchak. All rights reserved.
//

#import "SamplePlayListItem.h"


@implementation SamplePlayListItem

- (instancetype)initWithImage:(UIImage *)image artist:(NSString *)artist title:(NSString *)title {
    self = [super init];
    if (self) {
        _image = image;
        _artist = artist;
        _title = title;
    }
    return self;
}

@end
