//
//  PlayListHeaderView.m
//  RemixMusic
//
//  Created by Jesse Tomchak on 8/9/15.
//  Copyright © 2015 Jesse Tomchak. All rights reserved.
//

#import "PlayListHeaderView.h"

@implementation PlayListHeaderView

- (void)setPlaylistItem:(id<PlayListItem>)playListItem animated:(BOOL)animated {
    void (^updateBlock)() = ^ {
        self.artistLabel.text = [playListItem artist];
        self.songLabel.text = [playListItem title];
        self.blurredImageView.image = [playListItem image];
        self.imageView.image = [playListItem image];
    };
    
    if (animated) {
        
        UIView *prevState = [self snapshotViewAfterScreenUpdates:NO];
        [self addSubview:prevState];
        updateBlock();
        
        [UIView animateWithDuration:0.3
                         animations:^{
                             prevState.alpha = 0;
                         } completion:^(BOOL finished) {
                             [prevState removeFromSuperview];
                         }];
        
        //[UIView animateWithDuration:0.4 animations:updateBlock];
    } else {
        updateBlock();
    }
}

@end
