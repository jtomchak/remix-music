//
//  SamplePlayListItem.h
//  RemixMusic
//
//  Created by Jesse Tomchak on 8/9/15.
//  Copyright © 2015 Jesse Tomchak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlayListItem.h"

@interface SamplePlayListItem : NSObject <PlayListItem>

@property (nonatomic, readonly) UIImage *image;
@property (nonatomic, readonly) NSString *artist;
@property (nonatomic, readonly) NSString *title;

//init to take all properties
- (instancetype)initWithImage:(UIImage *)image artist:(NSString *)artist title:(NSString *)title;


@end
