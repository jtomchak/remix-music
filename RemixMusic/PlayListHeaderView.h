//
//  PlayListHeaderView.h
//  RemixMusic
//
//  Created by Jesse Tomchak on 8/9/15.
//  Copyright © 2015 Jesse Tomchak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayListItem.h"

@interface PlayListHeaderView : UICollectionReusableView

@property (nonatomic, weak) IBOutlet UIImageView *blurredImageView;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UILabel *songLabel;
@property (nonatomic, weak) IBOutlet UILabel *artistLabel;

- (void)setPlaylistItem:(id<PlayListItem>)playListItem animated:(BOOL)animated;

@end
