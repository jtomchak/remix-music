//
//  ViewController.h
//  RemixMusic
//
//  Created by Jesse Tomchak on 8/6/15.
//  Copyright (c) 2015 Jesse Tomchak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayerBar.h"

@interface ViewController : UIViewController <PlayerBarDelegate, UICollectionViewDelegate>


@end

