//
//  ViewController.m
//  RemixMusic
//
//  Created by Jesse Tomchak on 8/6/15.
//  Copyright (c) 2015 Jesse Tomchak. All rights reserved.
//

#import "ViewController.h"
#import "PlayListDataSource.h"
#import "SamplePlayListItem.h"
#import "PlayListHeaderView.h"
@import MediaPlayer;

@interface ViewController () <MPMediaPickerControllerDelegate>

@property (nonatomic, weak)IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) MPMediaItemCollection *playList;
@property (nonatomic, strong) MPMusicPlayerController *player;
@property (nonatomic, weak) IBOutlet PlayerBar *playerBar;
//@property (nonatomic, strong) UIBarButtonItem *playButton;
@property (nonatomic, strong) IBOutlet PlayListDataSource *playListDataSource;
@property (weak, nonatomic) IBOutlet UIView *headerContainerView;


@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.player = [[MPMusicPlayerController alloc ] init];
    
    self.collectionView.contentInset = UIEdgeInsetsMake(-64, 0, 44, 0);
    self.collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(-64, 0, 44, 0);
//     Used collectionView header for album artwork, changed over to standard view on top of collection
//    [self.collectionView registerNib:[UINib nibWithNibName:@"PlayListHeaderView" bundle:nil]
//          forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];
    UINib *headerNib = [UINib nibWithNibName:@"PlayListHeaderView" bundle:nil];
    NSArray *objects = [headerNib instantiateWithOwner:self options:nil];
    
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    CGSize itemSize = flowLayout.itemSize;
    itemSize.width = self.view.frame.size.width;
    flowLayout.itemSize = itemSize;
    
    
    PlayListHeaderView *header = [objects firstObject];
    [self.headerContainerView addSubview:header];
    
#if TARGET_IPHONE_SIMULATOR
    NSArray *items = @[
                       [[SamplePlayListItem alloc] initWithImage:[UIImage imageNamed:@"inutero.jpg"] artist:@"Nirvana" title:@"Heart-Shaped Box"],
                       [[SamplePlayListItem alloc] initWithImage:[UIImage imageNamed:@"inrainbows.jpg"] artist:@"Radiohead" title:@"House of Cards"],
                       [[SamplePlayListItem alloc] initWithImage:[UIImage imageNamed:@"aenima.jpg"] artist:@"Tool" title:@"Forty-six and Two"],
                       [[SamplePlayListItem alloc] initWithImage:[UIImage imageNamed:@"meddle.jpg"] artist:@"Pink Floyd" title:@"Echoes"],
                       [[SamplePlayListItem alloc] initWithImage:[UIImage imageNamed:@"andjustice.jpg"] artist:@"Metallica" title:@"One"],
                       [[SamplePlayListItem alloc] initWithImage:[UIImage imageNamed:@"bloodsugar.jpg"] artist:@"Red Hot Chili Peppers" title:@"Give it Away"],
                       [[SamplePlayListItem alloc] initWithImage:[UIImage imageNamed:@"inrainbows.jpg"] artist:@"Radiohead" title:@"House of Cards"],
                       [[SamplePlayListItem alloc] initWithImage:[UIImage imageNamed:@"ten.jpg"] artist:@"Pearl Jam" title:@"Ten"],
                       [[SamplePlayListItem alloc] initWithImage:[UIImage imageNamed:@"darkside.jpg"] artist:@"Pink Floyd" title:@"The Great Gig in the Sky"],
                       [[SamplePlayListItem alloc] initWithImage:[UIImage imageNamed:@"dummy.jpg"] artist:@"Portishead" title:@"Strangers"],
                       [[SamplePlayListItem alloc] initWithImage:[UIImage imageNamed:@"suburbs.jpg"] artist:@"Arcade Fire" title:@"Modern Man"],
                       [[SamplePlayListItem alloc] initWithImage:[UIImage imageNamed:@"housesoftheholy.jpg"] artist:@"Led Zeppelin" title:@"No Quarter"],
                       [[SamplePlayListItem alloc] initWithImage:[UIImage imageNamed:@"kindofblue.jpg"] artist:@"Miles Davis" title:@"Freddie Freeloader"]
                       ];
    self.playListDataSource.items = items;
    self.playerBar.enabled = YES;
#endif
    
    self.playListDataSource.playListHeaderView = header;
}

-(UIBarButtonItem *)playButtonItemForPlaybackState:(MPMusicPlaybackState)state {
    UIBarButtonSystemItem systemItem;
    if (state == MPMusicPlaybackStatePlaying) {
        systemItem = UIBarButtonSystemItemPlay;
    } else {
        systemItem = UIBarButtonSystemItemPause;
    }
    
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:systemItem target:self action:@selector(playPause:)];
    
    return buttonItem;
}

- (void)togglePlayPause {
    if (self.player.playbackState == MPMusicPlaybackStatePlaying) {
        NSLog(@"pausing player");
        [self.player pause];
        [self.playerBar setPlayButtonState:NO];
        
    } else {
        NSLog(@"playing player");
        [self.player play];
        [self.playerBar setPlayButtonState:YES];
    }
}

#pragma mark - Actions

- (IBAction)playPause:(id)sender {
    //built separate to be able to call programmatically
    [self togglePlayPause];
}

- (IBAction)addMusic:(id)sender {
    
    MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes:MPMediaTypeMusic];
    mediaPicker.prompt = @"Add music to Playlist";
    mediaPicker.delegate = self;
    mediaPicker.allowsPickingMultipleItems = YES;
    mediaPicker.showsCloudItems = YES;
    [self presentViewController:mediaPicker animated:YES completion:nil];
    
}

#pragma mark- MPMediaPickerControllerDelegate

-(void)mediaPicker:(nonnull MPMediaPickerController *)mediaPicker didPickMediaItems:(nonnull MPMediaItemCollection *)mediaItemCollection {
    NSLog(@"%@", NSStringFromSelector(_cmd));
        //empty playlist add media items
    if (!self.playList){
        self.playList = mediaItemCollection;
    } else {
        //append new itesm to existing array
        NSMutableArray *items = [NSMutableArray arrayWithCapacity:self.playList.count + mediaItemCollection.count];
        [items addObjectsFromArray:self.playList.items];
        [items addObjectsFromArray:mediaItemCollection.items];
        MPMediaItemCollection *collection = [MPMediaItemCollection collectionWithItems:items];
        self.playList = collection;
    }
    
    self.playerBar.enabled = YES;
    
    //quick logging to see if picked items are coming over AND appending
    int index = 1;
    for (MPMediaItem *item in self.playList.items) {
        NSLog(@"%d) %@ - %@", index++, item.artist, item.title);
    }
    
    [self.player setQueueWithItemCollection:self.playList];
    self.playListDataSource.items = self.playList.items;
    
    if (self.player.playbackState != MPMusicPlaybackStatePlaying) {
        //[self.player play];
        [self togglePlayPause];
    }
    
    
    //Dismiss VC
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)mediaPickerDidCancel:(nonnull MPMediaPickerController *)mediaPicker {
    NSLog(@"%@", NSStringFromSelector(_cmd));
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma -mark - PlayerBarDelegate

- (void)playerBarNextTrack:(PlayerBar *)playerBar {
    //move along in the currentTrack items
    self.playListDataSource.currentTrackIndex += 1;
#if !TARGET_IPHONE_SIMULATOR
    [self.player skipToNextItem];
#endif
}

- (void)playerBarPreviousTrack:(PlayerBar *)playerBar {
    //move to previous in currentTrack items
    self.playListDataSource.currentTrackIndex -= 1;
#if !TARGET_IPHONE_SIMULATOR
    [self.player skipToPreviousItem];
#endif
}

- (void)playerBarPlayPause:(PlayerBar *)playerBar {
    [self togglePlayPause];
}

#pragma -mark - UICollectionViewDelegate

- (void)collectionView:(nonnull UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    //if it is selected do nothing
    if (indexPath.row == self.playListDataSource.currentTrackIndex) {
        return;
    }
    
    self.playListDataSource.currentTrackIndex = indexPath.row;
#if !TARGET_IPHONE_SIMULATOR
    MPMediaItem *item = self.playList.items[indexPath.row];
    [self.player setNowPlayingItem:item];
    
    if (self.player.playbackState == MPMusicPlaybackStateStopped || self.player.playbackState == MPMusicPlaybackStatePaused) {
        [self.player play];
    }
#endif
}

@end
