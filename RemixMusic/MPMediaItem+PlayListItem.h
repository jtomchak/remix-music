//
//  MPMediaItem+PlayListItem.h
//  RemixMusic
//
//  Created by Jesse Tomchak on 8/9/15.
//  Copyright © 2015 Jesse Tomchak. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>

@interface MPMediaItem (PlayListItem)

@property (nonatomic, readonly) UIImage *image;

@end
