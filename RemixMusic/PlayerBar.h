//
//  PlayerBar.h
//  RemixMusic
//
//  Created by Jesse Tomchak on 8/8/15.
//  Copyright © 2015 Jesse Tomchak. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PlayerBarDelegate;



IB_DESIGNABLE
@interface PlayerBar : UIToolbar

@property (nonatomic, weak)IBOutlet id<PlayerBarDelegate> playerDarDelegate;

@property (nonatomic, assign)IBInspectable CGFloat spacing;
@property (nonatomic, assign) BOOL enabled;

- (void)setPlayButtonState:(BOOL)isPlaying;

@end


@protocol PlayerBarDelegate <NSObject>

- (void)playerBarPreviousTrack:(PlayerBar *)playerBar;
- (void)playerBarNextTrack:(PlayerBar *)playerBar;
- (void)playerBarPlayPause:(PlayerBar *)playerBar;

@end