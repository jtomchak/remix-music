//
//  PlayListDataSource.h
//  RemixMusic
//
//  Created by Jesse Tomchak on 8/9/15.
//  Copyright © 2015 Jesse Tomchak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayListItem.h" //Sample PlayList Items for Development
#import "PlayListHeaderView.h"

@interface PlayListDataSource : NSObject <UICollectionViewDataSource>

@property (nonatomic, weak) IBOutlet UICollectionView* collectionView;


@property (nonatomic, assign) NSInteger currentTrackIndex;
@property (nonatomic, strong) NSArray *items;
@property  (nonatomic, strong) PlayListHeaderView *playListHeaderView;



@end
