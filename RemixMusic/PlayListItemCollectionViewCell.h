//
//  PlayListItemCollectionViewCell.h
//  RemixMusic
//
//  Created by Jesse Tomchak on 8/9/15.
//  Copyright © 2015 Jesse Tomchak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayListItemCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak)IBOutlet UIImageView* imageView;
@property (nonatomic, weak)IBOutlet UILabel* artistLabel;
@property (nonatomic, weak)IBOutlet UILabel* songLabel;

@end
