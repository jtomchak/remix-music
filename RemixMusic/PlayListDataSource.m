//
//  PlayListDataSource.m
//  RemixMusic
//
//  Created by Jesse Tomchak on 8/9/15.
//  Copyright © 2015 Jesse Tomchak. All rights reserved.
//

#import "PlayListDataSource.h"
#import "PlayListItemCollectionViewCell.h"
#import "PlayListHeaderView.h"

@implementation PlayListDataSource

- (void)setPlayListHeaderView:(PlayListHeaderView *)playListHeaderView {
    _playListHeaderView = playListHeaderView;
    if (self.items.count > 0) {
    id<PlayListItem> item = self.items[_currentTrackIndex];
    [self.playListHeaderView setPlaylistItem:item animated:YES];
    }
}

- (void)setCurrentTrackIndex:(NSInteger)currentTrackIndex {
    if (currentTrackIndex >= (NSInteger)self.items.count) {
        currentTrackIndex = 0;
    } else if(currentTrackIndex < 0) {
        currentTrackIndex = self.items.count - 1;
    }
    
    NSIndexPath *oldIndexPath = [NSIndexPath indexPathForItem:_currentTrackIndex
                                                    inSection:0];
    NSIndexPath *newIndexPath = [NSIndexPath indexPathForItem:currentTrackIndex
                                                    inSection:0];
    
    _currentTrackIndex = currentTrackIndex;
    
    id<PlayListItem> item = self.items[_currentTrackIndex];
    [self.playListHeaderView setPlaylistItem:item animated:YES];
    
    if (self.items.count > _currentTrackIndex + 1) {
        NSIndexPath *nextTrackIndexPath = [NSIndexPath indexPathForItem:_currentTrackIndex + 1 inSection:0];
        [self.collectionView scrollToItemAtIndexPath:nextTrackIndexPath
                                    atScrollPosition:UICollectionViewScrollPositionTop
                                            animated:YES];
    } else if (self.items.count - 1 == _currentTrackIndex) {
        NSIndexPath *firstTrackIndexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        [self.collectionView scrollToItemAtIndexPath:firstTrackIndexPath
                                    atScrollPosition:UICollectionViewScrollPositionTop
                                            animated:YES];
    }
    
    [self.collectionView reloadItemsAtIndexPaths:@[oldIndexPath, newIndexPath]];
}

- (void)setItems:(NSArray *)items {
    _items = items;
    if (self.items.count > 0) {
        id<PlayListItem> item = self.items[_currentTrackIndex];
        [self.playListHeaderView setPlaylistItem:item animated:YES];
    }
    [self.collectionView reloadData];
    if (self.items.count > _currentTrackIndex + 1) {
        NSIndexPath *nextTrackIndexPath = [NSIndexPath indexPathForItem:_currentTrackIndex + 1 inSection:0];
        [self.collectionView scrollToItemAtIndexPath:nextTrackIndexPath atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
    }
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.items.count;
}

- (nonnull UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    PlayListItemCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    //able to fake in the simulator
    id<PlayListItem> playListItem = self.items[indexPath.row];
    cell.imageView.image = playListItem.image;
    cell.artistLabel.text = playListItem.artist;
    cell.songLabel.text = playListItem.title;
    
    if (indexPath.row == self.currentTrackIndex) {
            cell.backgroundColor = [UIColor darkGrayColor];
         } else {
             cell.backgroundColor = [UIColor blackColor];
         }
    
    return cell;
}



@end
