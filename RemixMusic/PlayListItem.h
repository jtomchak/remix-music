//
//  PlayListItem.h
//  RemixMusic
//
//  Created by Jesse Tomchak on 8/9/15.
//  Copyright © 2015 Jesse Tomchak. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PlayListItem <NSObject>

@property (nonatomic, readonly) UIImage *image;
@property (nonatomic, readonly) NSString *artist;
@property (nonatomic, readonly) NSString *title;


@end
